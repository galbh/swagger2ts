import { join } from "path";
import { renderFile as renderTemplateFile } from "template-file";
import { FileService, LogService } from "../services";
import Iterator from "./generator.iterator";
import { IGeneratorArgs, IProperty } from "./generator.models";
import { capitalize } from "./utils";

export default class EnumGenerator {
  public enums: { [name: string]: string[] } = {};
  private fileService: FileService;
  private logService: LogService;
  private destinationFolderName: string;
  private enumFileName: string;
  private commonGenerator: Iterator;

  constructor(args: IGeneratorArgs) {
    this.fileService = args.fileService;
    this.logService = args.logService;
    this.destinationFolderName = args.destinationFolderName;
    this.enumFileName = args.fileName;
    this.commonGenerator = new Iterator(args.definitions, this.writeEnum);
  }

  public generate = () => {
    this.commonGenerator.initiate();
    this.writeToFile();
  }

  private writeToFile = () => {
    if (!Object.keys(this.enums).length) { return; }
    const promises: Array<Promise<string>> = [];
    let stringEnums = "";

    Object.keys(this.enums).map((name, i) => {
      const promise = renderTemplateFile(
        join(__dirname, "../", "templates", "enum.ts"),
        {
          enum_name: name,
          enums: this.enums[name].map((t: string) => `${t} = '${t}'`).join(",\n  ")
        }
      )
        .then((s: string) => stringEnums += `${s}\n\n`);
      promises.push(promise);
    });

    Promise.all(promises)
      .finally(() => {
        this.fileService.writeToFile(this.enumFileName, stringEnums.slice(0, stringEnums.length - 1));
      })
      .then(() => {
        this.logService.log(`${this.destinationFolderName}/${this.enumFileName} was generated!`);
      });
  }

  private writeEnum = (p: IProperty, defName: string, propName: string) => {
    if (p.allOf) {
      const properties = p.allOf.find((a) => a.properties);

      if (properties) {
        this.writeEnum(properties, defName, propName);
      }
    }

    if (p.enum) {
      this.enums[capitalize(propName)] = p.enum;
    }

    const rulesProperties = p.properties && p.properties.rules && p.properties.rules.properties;

    if (rulesProperties) {
      Object.keys(rulesProperties).forEach((k) => this.writeEnum(rulesProperties[k], defName, k));
    }

    if (p.properties) {
      Object.keys(p.properties).forEach((k) => this.writeEnum(p.properties[k], defName, k));
    }

    if (p.items && p.items.properties) {
      Object.keys(p.items.properties).forEach((k) => {
        if (p.items && p.items.properties) {
          this.writeEnum(p.items.properties[k], defName, k);
        }
      });
    }
  }
}
