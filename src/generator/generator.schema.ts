import { FileService, LogService } from "../services";
import { IDefinitions, IGeneratorArgs } from "./generator.models";

export default class SchemaGenerator {
  private definitions: IDefinitions | undefined;
  private fileService: FileService;
  private logService: LogService;
  private destinationFolderName: string;
  private schemaFileName: string;

  constructor(args: IGeneratorArgs) {
    this.fileService = args.fileService;
    this.logService = args.logService;
    this.destinationFolderName = args.destinationFolderName;
    this.schemaFileName = args.fileName;
    this.definitions = args.definitions;
  }

  public generate = () => {
    this.fileService.writeToFile(this.schemaFileName, JSON.stringify(this.definitions, null, 2))
      .then(() => this.logService.log(`${this.destinationFolderName}/${this.schemaFileName} was generated!`))
      .catch((err: any) => { throw new Error(`Generator: generate schemas file failed: ${err}`); });
  }
}
