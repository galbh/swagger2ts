import { IDefinitions, IForEach, IItem, IProperty, ISchemaDefinition } from "./generator.models";
import { getSchemaName } from "./utils";

export default class Iterator {
  private definitions: IDefinitions;
  private forEach: IForEach;

  constructor(definitions: IDefinitions, forEach: any) {
    this.definitions = definitions;
    this.forEach = forEach;
  }

  public initiate = () => {
    Object.keys(this.definitions).forEach((defName) => {
      const definition: ISchemaDefinition = this.definitions[defName];
      this.exctractAllDefs(definition, defName);
    });
  }

  private extractRef = (ref: string) => {
    const definitionName = getSchemaName(ref);
    const definition: ISchemaDefinition = this.definitions[definitionName];

    if (!definition || (definition && !definition.properties)) {
      return;
    }

    Object.keys(definition.properties).forEach((propName) => {
      const property: IProperty = definition.properties[propName];
      this.forEach(property, definitionName, propName);
      if (property.items) {
        this.exctractItemsDef(property);
      }
    });
  }

  private extractOneOf = (oneOf: Array<{ $ref: string; }>) => {
    oneOf.forEach((item) => {
      const definitionName = getSchemaName(item.$ref);
      const definition: ISchemaDefinition = this.definitions[definitionName];
      if (definition.allOf) {
        definition.allOf.forEach((ao: ISchemaDefinition) => {
          if (ao.$ref) {
            return this.extractRef(ao.$ref);
          }
          if (ao.properties) {
            return this.exctractAllDefs(ao, definitionName);
          }
        });
      }
    });
  }

  private exctractItemsDef = (items: IItem) => {
    if (items.$ref) {
      this.extractRef(items.$ref);
    }
    if (items.oneOf) {
      this.extractOneOf(items.oneOf);
    }
  }

  private exctractAllDefs = (definition: ISchemaDefinition, defName: string) => {
    const properties: { [name: string]: IProperty } = definition.properties;
    if (properties) {
      Object.keys(properties).forEach((propName: string) => {
        const property: IProperty = properties[propName];
        this.forEach(property, defName, propName);
        if (property.items) {
          this.exctractItemsDef(property.items);
        }
        if (!property.type) {
          return this.exctractItemsDef(property);
        }
      });
    } else {
      this.forEach(definition, defName, "unknown");
    }
  }
}
