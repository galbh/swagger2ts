import promiseFinally from "promise.prototype.finally";
import { FileService, LogService } from "../services";
import EndPointsGenerator from "./generator.endpoints";
import EnumGenerator from "./generator.enum";
import InterfaceGenerator from "./generator.interface";
import { IGeneratorArgs, ISchemaDefinition } from "./generator.models";
import SchemaGenerator from "./generator.schema";
promiseFinally.shim();

export default class Generator {
  public destinationFolderName: string;
  private swagger: any;

  private endPointsFileName = "endpoints.ts";
  private interfacesFileName = "interfaces.ts";
  private enumFileName = "enums.ts";
  private schemasFileName = "schemas.json";

  private enumGenerator: EnumGenerator;
  private interfaceGenerator: InterfaceGenerator;
  private endpointGenerator: EndPointsGenerator;
  private schemaGenerator: SchemaGenerator;
  private definitions: { [name: string]: ISchemaDefinition };

  constructor(
    swagger: any,
    destinationFolderName: string,
    fileService: FileService,
    logService: LogService
  ) {
    if (!swagger) {
      throw new Error("Generator class swagger object is not defined");
    }
    this.swagger = swagger;
    this.destinationFolderName = destinationFolderName;
    this.definitions = this.getDefinitions();

    const args: IGeneratorArgs = {
      definitions: this.definitions,
      fileService,
      logService,
      destinationFolderName,
      fileName: ""
    };

    this.schemaGenerator = new SchemaGenerator({ ...args, fileName: this.schemasFileName });
    this.enumGenerator = new EnumGenerator({ ...args, fileName: this.enumFileName });
    this.interfaceGenerator = new InterfaceGenerator({ ...args, fileName: this.interfacesFileName });
    this.endpointGenerator = new EndPointsGenerator({
      ...args,
      fileName: this.endPointsFileName,
      paths: this.swagger.paths
    });
  }

  public generate() {
    this.enumGenerator.generate();
    const haveEnums: boolean = Object.keys(this.enumGenerator.enums).length > 0;
    this.interfaceGenerator.generate(haveEnums);
    this.endpointGenerator.generate();
    // this.schemaGenerator.generate();
  }

  private getDefinitions(): { [name: string]: ISchemaDefinition } {
    if (this.swagger.components) {
      return this.swagger.components.schemas;
    } else {
      return this.swagger.definitions;
    }
  }
}
