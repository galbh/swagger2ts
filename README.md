# Swagger to Interfaces
Super *lightweight* typescript interface generator.
> ✨Generate Typescript interfaces, enums and api endpoints straight from the swagger


**WHY ?!**

Swagger codegen allows you to generate a whole heavyweight full-blown client.<br />
Most of us frontend developers do not use it.<br />
We want to use our regular flow and loose the hassle of duplicating the server's schema.<br/>
**swagger-to-interfaces** is a lightweight library designed to auto generate *only* data which is useful.

---

### ⭐Installation
```
  npm install swagger-to-interfaces
```

### ⭐Usage
***
```
  swagger-to-interfaces --pathToFile=/path/to/json/swagger.json --destinationFolderName=./src/swagger2Ts
```

### ⭐Environment
♢ ***Arguments***: Include the following args on activation:


   - **--destinationFolderName**
    
   - **--pathToFile**  OR **--swaggerUrl**


***OR***

♢  ***.env file***: create a .env file at the root directory of your project with the following variables:


   - **DESTINATION_FOLDER_NAME**=./src/swagger2Ts

   - **SWAGGER_FILE_PATH**=./swagger.json **or** **SWAGGER_URL**=https://petstore.swagger.io/v2/swagger.json


### ⭐File generation
***
Three files will be created:
1. ♕ **interfaces.ts**
   ![interfaces.ts](https://gitlab.com/galbh/swagger2ts/-/raw/master/assets/interfaces.png)
   all model definitions in Typescript interfaces.
   

2. ♕ **endpoints.ts**
![endpoints.ts](https://gitlab.com/galbh/swagger2ts/-/raw/master/assets/endpoints.png)
    ```typescript
    [operationId: string]: {
       method: 'post' | 'get' | 'put' | 'delete';
       url?: string;
       getUrl?: (...params: any) => string};
       contentType?: string;
    }
    ```   


3. ♕ **enums.ts**
![enums.ts](https://gitlab.com/galbh/swagger2ts/-/raw/master/assets/enums.png)
all enum definitions.
   
---

### Common flow
♙ Get the swagger json from the server.

♘ Add it to the UI project (for example: /path/to/json/swagger).

♗ Add a hook to your package.json scripts section:

```
"build-swagger": "swagger-to-typescript --pathToFile=/path/to/json/swagger"
```

♕ Add this hook to your build process:

```
"prebuild": "npm run build-swagger"
```

♔ Make sure to commit your swagger.json, so the UI build is not dependent on external resources.

[view in npmjs.org](https://www.npmjs.com/package/swagger-to-interfaces)
